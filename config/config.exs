import Config

config :logger,
  level: :info

config :kaffe,
  producer: [
    heroku_kafka_env: true,
    topics: ["syringes_test"]
  ]
