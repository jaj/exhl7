defmodule Dispatcher do
  @behaviour MLLP.Dispatcher

  def generate_reply(message) do
    MLLP.Ack.get_ack_for_message(
      message,
      :application_accept
    )
    |> to_string()
    |> MLLP.Envelope.wrap_message()
  end

  def dispatch(:mllp_hl7, message, state) when is_binary(message) do
    reply = generate_reply(message)
    raw_msg = message |> HL7.Message.raw()
    message_type = raw_msg.header.message_type
    MessageHandler.parse_message(message_type, message)
    {:ok, %{state | reply_buffer: reply}}
  end
end
