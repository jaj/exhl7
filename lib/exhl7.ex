defmodule Exhl7 do
  @moduledoc """
  Documentation for `Exhl7`.
  """

  def start(_type, _args) do
    children = [
      {MLLP.Receiver,
       [
         port: 4090,
         dispatcher: Dispatcher,
         packet_framer: MLLP.DefaultPacketFramer
       ]}
    ]

    Supervisor.start_link(children, strategy: :one_for_one)
  end
end
