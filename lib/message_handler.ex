defmodule MessageHandler do
  import HL7.Query

  defp get_syringe_id(seg) do
    subid = get_part(seg, "4")
    parts = subid |> String.split(".")
    parts |> List.delete_at(-1) |> Enum.join(".")
  end

  defp extract_fields(seg) do
    field_identifiers = ["3.2", "5", "6.2"]
    fields_col = for p <- field_identifiers, do: get_parts(seg, p)
    syringe_numbers = for d <- get_data(seg), do: d.syringe
    [syringe_numbers | fields_col] |> List.zip()
  end

  defp fields_to_map({label, value, unit}) do
    %{label => %{:value => value, :unit => unit}}
  end

  def parse_message("ORU", message) do
    lst =
      message
      |> select("OBR [{OBX}]")
      |> select("OBX")
      |> data(&%{syringe: get_syringe_id(&1)})
      |> extract_fields()
      |> Enum.group_by(
        &(&1 |> elem(0)),
        &fields_to_map(&1 |> Tuple.delete_at(0))
      )

    IO.inspect(lst)
    {:ok, jsonresult} = JSON.encode(lst)
    # IO.puts(result)
    Kaffe.Producer.produce_sync("syringes_test", [{"syringe_hl7", jsonresult}])
    DateTime.utc_now() |> IO.puts()
  end
end
