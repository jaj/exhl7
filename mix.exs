defmodule Exhl7.MixProject do
  use Mix.Project

  def project do
    [
      app: :exhl7,
      version: "0.1.0",
      elixir: "~> 1.11",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {Exhl7, []},
      extra_applications: [:logger, :kaffe]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:mllp, "~> 0.8"},
      {:elixir_hl7, "~> 0.6"},
      {:json, "~> 1.4"},
      {:kaffe, "~> 1.22"}
      # {:distillery, "~> 2.1"}
    ]
  end
end
