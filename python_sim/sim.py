import sys
import time

from hl7 import client


def post_once(msg):
    with client.MLLPClient('127.0.0.1', 4090) as myclient:
        return myclient.send_message(msg)


if __name__ == '__main__':
    with open('icca.hl7', 'rb') as fp:
        msg = fp.read()

    while True:
        try:
            post_once(msg)
            sys.stdout.write("o")
        except:
            sys.stdout.write("x")
        sys.stdout.flush()
        time.sleep(5)
